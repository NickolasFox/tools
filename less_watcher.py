"""
single less file watcher, depends on lessc installed
"""
#!/usr/bin/env python
import os
import sys
from time import sleep

LESS_PATH = '/usr/bin/lessc'

def usage():
    print("%s file.less" % sys.argv[0])

def proceed_less(less, css):
    os.popen2('{binary} --yui-compress {less} > {css}'.format(
        less=less, css=css, binary=LESS_PATH)
    )

def main():
    if len(sys.argv) < 2:
        usage()
        sys.exit(-1)
    filename = sys.argv[1]
    if not filename.endswith('less'):
        print("%s is not less file extension file, please rename it")
        sys.exit(-2)
    if not os.path.exists(filename):
        print("%s does not exists" % filename)
        sys.exit(-3)
    css = filename.replace('.less', '.css')
    exists = os.path.exists(css)
    if not exists:
        # invoke less
        proceed_less(filename, css) 
    last_mstat = os.stat(filename).st_mtime
    while 1:
        mstat = os.stat(filename).st_mtime
        if mstat > last_mstat:
            proceed_less(less, css)
            last_mstat = mstat
        sleep(1)
if __name__ == '__main__':
    main()
